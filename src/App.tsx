import './App.css';
import SubTotal from './components/Subtotal/Subtotal';
import PickupSavings from './components/PickupSavings/PickupSavings';
import * as React from 'react';
import { Container } from 'react-bootstrap';
import TaxesFees from './components/TaxesFees/TaxesFees';
import EstimatedTotal from './components/EstimatedTotal/EstimatedTotal';
import ItemDetails from './components/ItemDetails/ItemDetails';
import PromoCodeDiscount from './components/PromoCode/PromoCode';
import { connect } from 'react-redux';
import handleChange from './actions/promoCodeActions';

interface AppState {
  total: number;
  PickupSavings: number;
  TaxesFees: number;
  EstimatedTotal: number;
  disablePromoButton: boolean;
  taxes: number;
}
interface AppProps {
  promoCode: string;
}

class App extends React.Component<AppProps, AppState> {
  constructor(props: any) {
    super(props);

    this.state = {
      total: 100,
      PickupSavings: -3.85,
      TaxesFees: 0,
      EstimatedTotal: 0,
      disablePromoButton: false,
      taxes: 5
    };
  }

  componentDidMount = () => {
    this.setState({
      taxes: (this.state.total + this.state.PickupSavings) * 0.0875
    },            () => {
      this.setState({
        EstimatedTotal: this.state.total + this.state.PickupSavings + this.state.taxes
      });
    });
  }

  giveDiscountHandler = () => {
    if (this.props.promoCode === 'DISCOUNT') {
      this.setState({
        EstimatedTotal: this.state.EstimatedTotal * 0.9
      },            () => {
        this.setState({
          disablePromoButton: true
        });
      }
      );
    }
  }

  render() {
    console.log(this.props);
    return (
      <div className="container">
        <Container className="purchase-card">
          <SubTotal price={this.state.total.toFixed(2)} />
          <PickupSavings price={this.state.PickupSavings.toString()} />
          <TaxesFees taxes={this.state.TaxesFees.toFixed(2)} />
          <hr />
          <EstimatedTotal price={this.state.EstimatedTotal.toFixed(2)} />
          <ItemDetails price={this.state.EstimatedTotal.toFixed(2)} />
          <hr />
          <PromoCodeDiscount
            giveDiscount={this.giveDiscountHandler}
            isDisabled={this.state.disablePromoButton}
            promoCodeString={this.props.promoCode}
          />
          <hr />
        </Container>
      </div>
    );
  }
}

const mapStateToProps = (state: any) => ({
  promoCode: state.promoCode.value
});

export default connect(mapStateToProps, { handleChange })(App);
