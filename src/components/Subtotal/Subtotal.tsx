import * as React from 'react';
import { Row, Col } from 'react-bootstrap';

interface SubtotalState {
    
}

interface SubtotalProps {
    price: string;
}

class SubTotal extends React.Component<SubtotalProps, SubtotalState> {
    render() {
        return(
            <Row className="show-grid">
            <Col md={6}>Subtotal</Col>
            <Col md={6}>{`$${this.props.price}`}</Col>
            </Row>
        );
    }
}
export default SubTotal;