import * as React from 'react';
import { Row, Col } from 'react-bootstrap';

interface TaxesFeesState {
    total: number;
  }
interface TaxesFeesProps {
    taxes: string;
}
class TaxesFees extends React.Component<TaxesFeesProps, TaxesFeesState> {
    render() {
        return(
            <Row className="show-grid">
             <Col md={6}>Est. taxes & fees (based on 94085) </Col>
             <Col md={6}>{`$${this.props.taxes}`} </Col>
            </Row>
        );

    }
}
export default TaxesFees;